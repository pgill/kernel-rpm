#!/usr/bin/env bash
set -eu -o pipefail

echo shell options: $-
INTERACTIVE_ARG="--assumeyes"
[[ $- == *i* ]] && INTERACTIVE_ARG=""
SUDO_CMD="sudo"
[[ $(id --user) == 0 ]] && SUDO_CMD=""

SCRIPT_DIRECTORY="$(cd "$(dirname "$0")";pwd)"
pushd "$SCRIPT_DIRECTORY"
trap popd EXIT

SPEC_FILE="$(find . -maxdepth 1 -type f -name '*.spec' | head -z -n 1)"
if [ -z "$SPEC_FILE" ]; then
  echo "spec file not found"
  exit 0
fi
rpmdev-setuptree
SPEC_FILE_TARGET="${HOME}/rpmbuild/SPECS/$(basename "$SPEC_FILE")"
cp --force "$SPEC_FILE" "$SPEC_FILE_TARGET"

download_source() {
  source_file="$1"
  source_checksum="$(grep -oP '(?<=SHA512 \('$source_file'\) = )(.+)' sources)"
  if [ -z "$source_checksum" ]; then
    echo "$source_file : unable to locate SHA512 checksum"
    exit 1
  fi
  source_url="https://src.fedoraproject.org/lookaside/pkgs/kernel/${source_file}/sha512/${source_checksum}/${source_file}"
  curl --output "$source_file" "$source_url"
}

for sf in $(spectool --list-files "$SPEC_FILE_TARGET" | awk '{ print $2 }' | grep --invert --extended-regexp '^http(s)?://'); do
  if [ ! -e "$sf" ]; then
    echo "$sf : source not found locally, downloading it."
    download_source $sf
  fi
  cp --force $sf ~/rpmbuild/SOURCES/
done
spectool --get-files --sourcedir "$SPEC_FILE_TARGET"
$SUDO_CMD dnf builddep --assumeyes --spec "$SPEC_FILE_TARGET"
rpmbuild -bb "$SPEC_FILE_TARGET"
find ~/rpmbuild/RPMS
